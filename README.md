# Flisol2019

FLISOL 2019, evento desarrollado en la Universidad Distrital Francisco José de Caldas. 

Se incluyen diferentes formatos (impresión(CMYK),PDF, TIF, Digital(RGB), SVG, etc.)

#### Contenido

+ Afiche Oficial
	+ Tipografía oficial
	+ Afiche oficial
+ Agenda
    + Agenda General
    + Agenda individual
	+ Agenda por pisos
	+ Conferencias
	+ Talleres
+ Banners Para redes sociales
+ Conferencistas internacionales
+ Convocatoria Conferencistas
+ Escarapelas
	+ Fondo Blanco
	+ Fondo Negro
+ Invitación General (Nivel nacional)
	+ Instagram
	+ Faceboook
	+ Twitter
+ Invitación Local (únicamente Bogotá) 
	+ Instagram
	+ Faceboook
	+ Twitter
+ Libreta
+ Logo en negativo
+ Papelería
+ Base publicación planos
+ Taller Políticas Publicas
	+ Redes sociales
+ Volantes
	+ Jornada de instalación
	+ Evento

Desarrollo a cargo de: 

Código  | Nombre
------------- | -------------
20181020081  | Cristhian M. Yara P.
